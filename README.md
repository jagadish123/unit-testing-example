Use this with a pinch of salt.

The `index.js` file simplifies the original function written by you guys which will
be async, to be sync. However, it should produce identical data structures as a result.

If you dig a little into the the other functions there might be a way to mock or stub the actual
database object. also note that the `index.js` is rigged with fail the exists check at random intervels
which intern returns a new token., which in my case will never fail, yours might

from my judgement, you will be testing three things

1. Fails when context has no auth
2. Returns a token when auth is present and the token is valid
3. Returns a token when auth is present and the token has expired, hence we need to get a new token

this is a good example of working with promises and mocha
https://wietse.loves.engineering/testing-promises-with-mocha-90df8b7d2e35

your test case will look something like this:
```
it('returns existing token, if valid', function(done){
	const p = module.getOAuthToken({auth: {}});
	p.then(
		function(token_data) {
			assert.equal(token_data.hasAttribute("token"), true);
		}
	);
})
```

for expiring the token you can just re-write the isValid function in the test.js to explicitly return false :D
```
const myModule = require("../");
..
..
..
myModule.isValid = () => {
	return false;
}


//start test code
describe(
	..
	..
)
```


`exports.getOAuthToken = functions.https.onCall((data, context) => {...}`
in that funcion declaration data looks like its never used in the body ? anyways.. Im sure Anita can take it from here.. i might not be available to take the call tomorrow. (i will try to).. :)
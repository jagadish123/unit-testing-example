// Simplified version
const x = Math.random()
console.log(x);

const isValid = (dateStamp) => {
  if(dateStamp === "20190323") {
    return true;
  } else {
    return false;
  }
}

const newCreds = {token: "1234678-adttertrtretr32-adxggadd", expireTime: "20190323"};

// stub for doc
const tokenObj = {
  get: () => {
    return data;
  },
  exists: () => {
    if(x > 0.5) {
      return true;
    } else {
      return false;
    }
  },
  data: () => {
    tkn = "xxwtaxx-adttertrtretr32-adxggadd"
    if(x > 0.5) {
      return {token: tkn, expireTime: "20190323"};
    } else {
      return {token: tkn, expireTime: "20190322"};
    }
  }
}

// stub for db let docRef = db.collection('DialogflowTokens').doc('OauthToken');
const db = {
  collections: () => { 
    return {
      doc: () => { return tokenObj }
    }
  }
};

// the actual function which matters, over simplified but does somewhat what your
// code does
GetOAuthToken = (context) => {
  if(!context.auth) {
    throw new Error('Needs to be a vaild session');
  }
  // Get token
  let tkObj = db.collections("DialogflowTokens").doc("OauthToken");
  // your code will exit here and proceed with the .get() & .then()
  // im simplyfying that for the purposes of the demo
  if (tkObj.exists && isValid(tkObj.data().expireTime)) {
    return tkObj.data();
  } else {
    return newCreds;
  }
}

exports.getAuthToken = GetOAuthToken;
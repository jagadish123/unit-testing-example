const assert = require('assert');
let rainbow = require("../");


describe('Rainbows', function(done){
	it("should return a new token, when the auth is provided", function(){
		assert.equal(rainbow.getAuthToken({auth: {}}).token.length, 32);
	})
	it("should throw error, when no auth is present", function(){
		assert.throws(() => rainbow.getAuthToken({}), Error);
	})
})